'use strict';

const purify = require('purify-css');
const request = require('request-promise-native');
const tmp = require('tmp-promise');
const fs = require('fs');

class PurifyCSS {
	constructor(config) {
		this.config = config.plugins.purifycss || {};
	}
  optimize(file) {
  	try {
  	    if (!this.config || !this.config.options || !this.config.content) {
  	        return Promise.resolve(file);
  	    }
		const options = this.config.options;
		const promises = this.config.content.map(function (c) {
			if (c.startsWith('http')) {
				return request.get(c).then(function(response) {
					return tmp.file().then(function (tmpFile) {
						return new Promise(function (resolve, reject) {
						    console.log(`${tmpFile.path} ${response.length}`)
							const writeStream = fs.createWriteStream(tmpFile.path);
							writeStream.write(response);
							resolve(tmpFile.path);
						});
					})
				}).catch(function (error) {
					    return Promise.reject(error);
				});
			} else {
				return Promise.resolve(c);
			}
		});
		return Promise.all(promises).then(function (content) {
		    console.log(content);
			console.log(`Original CSS is ${file.data.length} bytes`);
	  		const purified = purify(content, file.data, options);
			return Promise.resolve(purified);
		});
  	} catch (error) {
  		return Promise.reject(error);
  	}
  }
}

PurifyCSS.prototype.brunchPlugin = true;
PurifyCSS.prototype.type = 'stylesheet';
PurifyCSS.prototype.defaultEnv = '*';
//PurifyCSS.prototype.extension = 'css';

module.exports = PurifyCSS;